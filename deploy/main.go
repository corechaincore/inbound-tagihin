package main

import (
	"gopkg.in/mgo.v2"
	"crypto/tls"
	"net"
	"fmt"
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"strings"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"time"
)

const PORT_NUMBER = 6060

func main() {
	fmt.Println("==== OTP services ====")
	fmt.Println(fmt.Sprintf(" Run on Port :%d", PORT_NUMBER))
	fmt.Println("======================")
	mdb := conn()
	defer mdb.Close()
	mux := http.NewServeMux()
	mux.HandleFunc("/apiv1/tagihin/inbound", inbound(mdb))
	mux.HandleFunc("/apiv1/tagihin/report/ref/", getDataByRef(mdb))
	mux.HandleFunc("/apiv1/tagihin/report/date/", report(mdb))
	mux.HandleFunc("/health", health)
	http.ListenAndServe(fmt.Sprintf(":%d", PORT_NUMBER), mux)
}

func health(w http.ResponseWriter, r *http.Request){
	/*p := message{Content:"STATUS : HEALTH"}
	t,_ := template.ParseFiles("deploy/index.html");
	t.Execute(w,p);*/

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"HEALTH"}`))
}

func conn() *mgo.Session {
	var mongoURI = "mongodb://corechain:corechaindb@corechain-shard-00-00-jqy0s.mongodb.net:27017,corechain-shard-00-01-jqy0s.mongodb.net:27017,corechain-shard-00-02-jqy0s.mongodb.net:27017/tagihin?replicaSet=corechain-shard-0&authSource=admin"
	dialInfo, err := mgo.ParseURL(mongoURI)
	if err != nil {
		panic(err)
	}

	//Below part is similar to above.
	tlsConfig := &tls.Config{}
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	session, err := mgo.DialWithInfo(dialInfo)
	session.SetMode(mgo.Monotonic, true)
	return session
}

func inbound(session *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"Service Unavailable"}`
		body,err := ioutil.ReadAll(r.Body)
		data, _ := jsonparser.GetString(body, "data")
		referenceCode, _ := jsonparser.GetString(body, "reference")
		dataArr := strings.Split(data,"|")

		unixDate, err := strconv.ParseInt(dataArr[3], 10, 64)
		if err != nil {
			panic(err)
		}

		tm := time.Unix(unixDate, 0)
		datetime := tm.Format("20060102")
		indatetime,_ := strconv.Atoi(datetime)
		objMap := make(map[string]interface{})
		objMap["phonecollector"] = dataArr[0]
		objMap["phonebilled"] = dataArr[1]
		objMap["amount"],_ = strconv.Atoi(dataArr[2])
		objMap["timestamp"] = unixDate
		objMap["dates"] = indatetime
		objMap["description"] = dataArr[4]
		objMap["referencecode"] = referenceCode

		//println(string(body))
		c := session.DB("tagihin").C("transaction")

		err = c.Insert(objMap)
		if err != nil {
			message = `{"message":"`+strings.Replace(err.Error(),"\"","",-1)+`"}`
		}else{
			code = http.StatusOK
			message = `{"message":"Sukses Simpan Data"}`
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
		/*result := Trx{}
		err = c.Find(bson.M{"name": "Fadhil"}).One(&result)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Phone:", result.PhoneCollector)*/

	}
}

func getDataByRef(session *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		code := http.StatusInternalServerError
		message := `{"message":"Service Unavailable"}`
		c := session.DB("tagihin").C("transaction")
		uri := r.RequestURI
		arrUri := strings.Split(uri,"/")
		refCode := arrUri[5]
		result := make(map[string]interface{})
		err := c.Find(bson.M{"referencecode": ""+refCode+""}).One(&result)
		if err != nil {
			code = http.StatusNotFound
			message = `{"message":"Data Not Found"}`
		}else{
			code = http.StatusOK
			dates := fmt.Sprintf("%v", result["dates"])
			date := fmt.Sprintf("%v", result["timestamp"])
			datess,_ := strconv.ParseInt(date, 10, 64)
			tm := time.Unix(datess, 0)
			times := tm.Format("Mon Jan _2 15:04:05 2006")

			message = fmt.Sprintf(`{"referencecode":"%v","phonecollector":"%v","phonebilled":"%v","timestamp":"%v","datetime":%v,"amount":%v,"description":"%v"}`,result["referencecode"],result["phonecollector"],result["phonebilled"],times,dates,result["amount"],result["description"])
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func report(session *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		code := http.StatusNotFound
		message := `{"message":"Data Not Found"}`
		c := session.DB("tagihin").C("transaction")
		uri := r.RequestURI
		result := make(map[string]interface{})
		arrUri := strings.Split(uri,"/")
		start,end,res := validationReport(arrUri)
		if(res){

			starts := convertDate(start,0)
			ends := convertDate(end,1)

			iter := c.Find(bson.M{
				"$and": []interface{}{
					bson.M{"dates": bson.M{"$gte": starts}},
					bson.M{"dates": bson.M{"$lte": ends}},
				},
			}).Iter()
			var body = ""
			for iter.Next(&result) {
				dates := fmt.Sprintf("%v", result["dates"])
				date := fmt.Sprintf("%v", result["timestamp"])
				datess,_ := strconv.ParseInt(date, 10, 64)
				tm := time.Unix(datess, 0)
				times := tm.Format("Mon Jan _2 15:04:05 2006")
				if strings.Compare(body, "") != 0 {
					body = body + ","
				}
				body = body + fmt.Sprintf(`{"referencecode":"%v","phonecollector":"%v","phonebilled":"%v","timestamp":"%v","datetime":%v,"amount":%v,"description":"%v"}`,result["referencecode"],result["phonecollector"],result["phonebilled"],times,dates,result["amount"],result["description"])
			}
			if(body == ""){
				code = http.StatusNotFound
				message = `{"message":"Data Not Found"}`
			}else{
				message = `{"data":[` + body + `]}`
				code = http.StatusOK
			}
		}

		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func validationReport(param []string)(start int,end int,result bool){
	result = false
	arrLen := len(param)
	if(arrLen == 7){
		result = true
		start,_ =  strconv.Atoi(fmt.Sprintf("%v",param[5]))
		end,_ = strconv.Atoi(fmt.Sprintf("%v",param[6]))
	}else if(arrLen == 6 && fmt.Sprintf("%v",param[5]) != ""){
		result = true
		start,_ =  strconv.Atoi(fmt.Sprintf("%v",param[5]))
		end,_ = strconv.Atoi(fmt.Sprintf("%v",param[5]))
	}

	return start,end,result
}

func convertDate(dates int, types int)(result int){
	strDates := strconv.Itoa(dates)
	dateLen := len(strDates)
	if(dateLen < 8){
		var strResult = ""
		var itr = ""
		if(types == 0){
			itr = "0"
		}else{
			itr = "9"
		}
		margin := 8 - dateLen
		for i := 0; i < margin; i++ {
			strResult =  strResult + itr
		}
		result,_ = strconv.Atoi(strDates+strResult)
	}else{
		result = dates
	}
	return result
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
